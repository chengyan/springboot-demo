package com.example.rabbitmq.message;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @Autowired
    HelloSender helloSender;

    @RequestMapping("/test")
    public void test(){
        helloSender.send();
    }
}
