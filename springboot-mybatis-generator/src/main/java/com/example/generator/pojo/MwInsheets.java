package com.example.generator.pojo;

import java.util.Date;

public class MwInsheets {
    private String insheetno;

    private String hosnum;

    private String osheetno;

    private String whouseid;

    private String whousename;

    private String intype;

    private Integer rsheetnum;

    private String rsheetsdes;

    private String unitid;

    private String unitname;

    private String sheetman;

    private String sheetmname;

    private Date sheetdate;

    private String checkman;

    private String checkmname;

    private Date checkdate;

    private String regman;

    private String regmname;

    private Date regdate;

    private String paysheetno;

    private String payman;

    private String paymname;

    private Date paydate;

    private String cancelflag;

    private String canceldes;

    private String comments;

    private String applybuyunit;

    private String applybuysheetid;

    private Date chgdate;

    private String docforred;

    private String receiveid;

    private String orderno;

    private String outinfitid;

    private Date purchasedate;

    private Date createdate;

    private String invoiceNo;

    private Date tracelog;

    private String purchaseway;

    private String docforredtype;

    private String paymentstatus;

    private String paymentcheckman;

    private String paymentcheckmname;

    private String paymentcheckdate;

    private String financestatus;

    private String financecheckman;

    private String financecheckmname;

    private String financecheckdate;

    private String shippingorderno;

    private String jcuploadflag;

    public String getInsheetno() {
        return insheetno;
    }

    public void setInsheetno(String insheetno) {
        this.insheetno = insheetno == null ? null : insheetno.trim();
    }

    public String getHosnum() {
        return hosnum;
    }

    public void setHosnum(String hosnum) {
        this.hosnum = hosnum == null ? null : hosnum.trim();
    }

    public String getOsheetno() {
        return osheetno;
    }

    public void setOsheetno(String osheetno) {
        this.osheetno = osheetno == null ? null : osheetno.trim();
    }

    public String getWhouseid() {
        return whouseid;
    }

    public void setWhouseid(String whouseid) {
        this.whouseid = whouseid == null ? null : whouseid.trim();
    }

    public String getWhousename() {
        return whousename;
    }

    public void setWhousename(String whousename) {
        this.whousename = whousename == null ? null : whousename.trim();
    }

    public String getIntype() {
        return intype;
    }

    public void setIntype(String intype) {
        this.intype = intype == null ? null : intype.trim();
    }

    public Integer getRsheetnum() {
        return rsheetnum;
    }

    public void setRsheetnum(Integer rsheetnum) {
        this.rsheetnum = rsheetnum;
    }

    public String getRsheetsdes() {
        return rsheetsdes;
    }

    public void setRsheetsdes(String rsheetsdes) {
        this.rsheetsdes = rsheetsdes == null ? null : rsheetsdes.trim();
    }

    public String getUnitid() {
        return unitid;
    }

    public void setUnitid(String unitid) {
        this.unitid = unitid == null ? null : unitid.trim();
    }

    public String getUnitname() {
        return unitname;
    }

    public void setUnitname(String unitname) {
        this.unitname = unitname == null ? null : unitname.trim();
    }

    public String getSheetman() {
        return sheetman;
    }

    public void setSheetman(String sheetman) {
        this.sheetman = sheetman == null ? null : sheetman.trim();
    }

    public String getSheetmname() {
        return sheetmname;
    }

    public void setSheetmname(String sheetmname) {
        this.sheetmname = sheetmname == null ? null : sheetmname.trim();
    }

    public Date getSheetdate() {
        return sheetdate;
    }

    public void setSheetdate(Date sheetdate) {
        this.sheetdate = sheetdate;
    }

    public String getCheckman() {
        return checkman;
    }

    public void setCheckman(String checkman) {
        this.checkman = checkman == null ? null : checkman.trim();
    }

    public String getCheckmname() {
        return checkmname;
    }

    public void setCheckmname(String checkmname) {
        this.checkmname = checkmname == null ? null : checkmname.trim();
    }

    public Date getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(Date checkdate) {
        this.checkdate = checkdate;
    }

    public String getRegman() {
        return regman;
    }

    public void setRegman(String regman) {
        this.regman = regman == null ? null : regman.trim();
    }

    public String getRegmname() {
        return regmname;
    }

    public void setRegmname(String regmname) {
        this.regmname = regmname == null ? null : regmname.trim();
    }

    public Date getRegdate() {
        return regdate;
    }

    public void setRegdate(Date regdate) {
        this.regdate = regdate;
    }

    public String getPaysheetno() {
        return paysheetno;
    }

    public void setPaysheetno(String paysheetno) {
        this.paysheetno = paysheetno == null ? null : paysheetno.trim();
    }

    public String getPayman() {
        return payman;
    }

    public void setPayman(String payman) {
        this.payman = payman == null ? null : payman.trim();
    }

    public String getPaymname() {
        return paymname;
    }

    public void setPaymname(String paymname) {
        this.paymname = paymname == null ? null : paymname.trim();
    }

    public Date getPaydate() {
        return paydate;
    }

    public void setPaydate(Date paydate) {
        this.paydate = paydate;
    }

    public String getCancelflag() {
        return cancelflag;
    }

    public void setCancelflag(String cancelflag) {
        this.cancelflag = cancelflag == null ? null : cancelflag.trim();
    }

    public String getCanceldes() {
        return canceldes;
    }

    public void setCanceldes(String canceldes) {
        this.canceldes = canceldes == null ? null : canceldes.trim();
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments == null ? null : comments.trim();
    }

    public String getApplybuyunit() {
        return applybuyunit;
    }

    public void setApplybuyunit(String applybuyunit) {
        this.applybuyunit = applybuyunit == null ? null : applybuyunit.trim();
    }

    public String getApplybuysheetid() {
        return applybuysheetid;
    }

    public void setApplybuysheetid(String applybuysheetid) {
        this.applybuysheetid = applybuysheetid == null ? null : applybuysheetid.trim();
    }

    public Date getChgdate() {
        return chgdate;
    }

    public void setChgdate(Date chgdate) {
        this.chgdate = chgdate;
    }

    public String getDocforred() {
        return docforred;
    }

    public void setDocforred(String docforred) {
        this.docforred = docforred == null ? null : docforred.trim();
    }

    public String getReceiveid() {
        return receiveid;
    }

    public void setReceiveid(String receiveid) {
        this.receiveid = receiveid == null ? null : receiveid.trim();
    }

    public String getOrderno() {
        return orderno;
    }

    public void setOrderno(String orderno) {
        this.orderno = orderno == null ? null : orderno.trim();
    }

    public String getOutinfitid() {
        return outinfitid;
    }

    public void setOutinfitid(String outinfitid) {
        this.outinfitid = outinfitid == null ? null : outinfitid.trim();
    }

    public Date getPurchasedate() {
        return purchasedate;
    }

    public void setPurchasedate(Date purchasedate) {
        this.purchasedate = purchasedate;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo == null ? null : invoiceNo.trim();
    }

    public Date getTracelog() {
        return tracelog;
    }

    public void setTracelog(Date tracelog) {
        this.tracelog = tracelog;
    }

    public String getPurchaseway() {
        return purchaseway;
    }

    public void setPurchaseway(String purchaseway) {
        this.purchaseway = purchaseway == null ? null : purchaseway.trim();
    }

    public String getDocforredtype() {
        return docforredtype;
    }

    public void setDocforredtype(String docforredtype) {
        this.docforredtype = docforredtype == null ? null : docforredtype.trim();
    }

    public String getPaymentstatus() {
        return paymentstatus;
    }

    public void setPaymentstatus(String paymentstatus) {
        this.paymentstatus = paymentstatus == null ? null : paymentstatus.trim();
    }

    public String getPaymentcheckman() {
        return paymentcheckman;
    }

    public void setPaymentcheckman(String paymentcheckman) {
        this.paymentcheckman = paymentcheckman == null ? null : paymentcheckman.trim();
    }

    public String getPaymentcheckmname() {
        return paymentcheckmname;
    }

    public void setPaymentcheckmname(String paymentcheckmname) {
        this.paymentcheckmname = paymentcheckmname == null ? null : paymentcheckmname.trim();
    }

    public String getPaymentcheckdate() {
        return paymentcheckdate;
    }

    public void setPaymentcheckdate(String paymentcheckdate) {
        this.paymentcheckdate = paymentcheckdate == null ? null : paymentcheckdate.trim();
    }

    public String getFinancestatus() {
        return financestatus;
    }

    public void setFinancestatus(String financestatus) {
        this.financestatus = financestatus == null ? null : financestatus.trim();
    }

    public String getFinancecheckman() {
        return financecheckman;
    }

    public void setFinancecheckman(String financecheckman) {
        this.financecheckman = financecheckman == null ? null : financecheckman.trim();
    }

    public String getFinancecheckmname() {
        return financecheckmname;
    }

    public void setFinancecheckmname(String financecheckmname) {
        this.financecheckmname = financecheckmname == null ? null : financecheckmname.trim();
    }

    public String getFinancecheckdate() {
        return financecheckdate;
    }

    public void setFinancecheckdate(String financecheckdate) {
        this.financecheckdate = financecheckdate == null ? null : financecheckdate.trim();
    }

    public String getShippingorderno() {
        return shippingorderno;
    }

    public void setShippingorderno(String shippingorderno) {
        this.shippingorderno = shippingorderno == null ? null : shippingorderno.trim();
    }

    public String getJcuploadflag() {
        return jcuploadflag;
    }

    public void setJcuploadflag(String jcuploadflag) {
        this.jcuploadflag = jcuploadflag == null ? null : jcuploadflag.trim();
    }
}