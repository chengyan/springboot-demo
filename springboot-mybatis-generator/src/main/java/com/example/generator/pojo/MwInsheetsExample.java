package com.example.generator.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MwInsheetsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public MwInsheetsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andInsheetnoIsNull() {
            addCriterion("INSHEETNO is null");
            return (Criteria) this;
        }

        public Criteria andInsheetnoIsNotNull() {
            addCriterion("INSHEETNO is not null");
            return (Criteria) this;
        }

        public Criteria andInsheetnoEqualTo(String value) {
            addCriterion("INSHEETNO =", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoNotEqualTo(String value) {
            addCriterion("INSHEETNO <>", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoGreaterThan(String value) {
            addCriterion("INSHEETNO >", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoGreaterThanOrEqualTo(String value) {
            addCriterion("INSHEETNO >=", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoLessThan(String value) {
            addCriterion("INSHEETNO <", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoLessThanOrEqualTo(String value) {
            addCriterion("INSHEETNO <=", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoLike(String value) {
            addCriterion("INSHEETNO like", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoNotLike(String value) {
            addCriterion("INSHEETNO not like", value, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoIn(List<String> values) {
            addCriterion("INSHEETNO in", values, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoNotIn(List<String> values) {
            addCriterion("INSHEETNO not in", values, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoBetween(String value1, String value2) {
            addCriterion("INSHEETNO between", value1, value2, "insheetno");
            return (Criteria) this;
        }

        public Criteria andInsheetnoNotBetween(String value1, String value2) {
            addCriterion("INSHEETNO not between", value1, value2, "insheetno");
            return (Criteria) this;
        }

        public Criteria andHosnumIsNull() {
            addCriterion("HOSNUM is null");
            return (Criteria) this;
        }

        public Criteria andHosnumIsNotNull() {
            addCriterion("HOSNUM is not null");
            return (Criteria) this;
        }

        public Criteria andHosnumEqualTo(String value) {
            addCriterion("HOSNUM =", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumNotEqualTo(String value) {
            addCriterion("HOSNUM <>", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumGreaterThan(String value) {
            addCriterion("HOSNUM >", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumGreaterThanOrEqualTo(String value) {
            addCriterion("HOSNUM >=", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumLessThan(String value) {
            addCriterion("HOSNUM <", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumLessThanOrEqualTo(String value) {
            addCriterion("HOSNUM <=", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumLike(String value) {
            addCriterion("HOSNUM like", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumNotLike(String value) {
            addCriterion("HOSNUM not like", value, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumIn(List<String> values) {
            addCriterion("HOSNUM in", values, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumNotIn(List<String> values) {
            addCriterion("HOSNUM not in", values, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumBetween(String value1, String value2) {
            addCriterion("HOSNUM between", value1, value2, "hosnum");
            return (Criteria) this;
        }

        public Criteria andHosnumNotBetween(String value1, String value2) {
            addCriterion("HOSNUM not between", value1, value2, "hosnum");
            return (Criteria) this;
        }

        public Criteria andOsheetnoIsNull() {
            addCriterion("OSHEETNO is null");
            return (Criteria) this;
        }

        public Criteria andOsheetnoIsNotNull() {
            addCriterion("OSHEETNO is not null");
            return (Criteria) this;
        }

        public Criteria andOsheetnoEqualTo(String value) {
            addCriterion("OSHEETNO =", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoNotEqualTo(String value) {
            addCriterion("OSHEETNO <>", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoGreaterThan(String value) {
            addCriterion("OSHEETNO >", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoGreaterThanOrEqualTo(String value) {
            addCriterion("OSHEETNO >=", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoLessThan(String value) {
            addCriterion("OSHEETNO <", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoLessThanOrEqualTo(String value) {
            addCriterion("OSHEETNO <=", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoLike(String value) {
            addCriterion("OSHEETNO like", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoNotLike(String value) {
            addCriterion("OSHEETNO not like", value, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoIn(List<String> values) {
            addCriterion("OSHEETNO in", values, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoNotIn(List<String> values) {
            addCriterion("OSHEETNO not in", values, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoBetween(String value1, String value2) {
            addCriterion("OSHEETNO between", value1, value2, "osheetno");
            return (Criteria) this;
        }

        public Criteria andOsheetnoNotBetween(String value1, String value2) {
            addCriterion("OSHEETNO not between", value1, value2, "osheetno");
            return (Criteria) this;
        }

        public Criteria andWhouseidIsNull() {
            addCriterion("WHOUSEID is null");
            return (Criteria) this;
        }

        public Criteria andWhouseidIsNotNull() {
            addCriterion("WHOUSEID is not null");
            return (Criteria) this;
        }

        public Criteria andWhouseidEqualTo(String value) {
            addCriterion("WHOUSEID =", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidNotEqualTo(String value) {
            addCriterion("WHOUSEID <>", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidGreaterThan(String value) {
            addCriterion("WHOUSEID >", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidGreaterThanOrEqualTo(String value) {
            addCriterion("WHOUSEID >=", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidLessThan(String value) {
            addCriterion("WHOUSEID <", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidLessThanOrEqualTo(String value) {
            addCriterion("WHOUSEID <=", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidLike(String value) {
            addCriterion("WHOUSEID like", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidNotLike(String value) {
            addCriterion("WHOUSEID not like", value, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidIn(List<String> values) {
            addCriterion("WHOUSEID in", values, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidNotIn(List<String> values) {
            addCriterion("WHOUSEID not in", values, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidBetween(String value1, String value2) {
            addCriterion("WHOUSEID between", value1, value2, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhouseidNotBetween(String value1, String value2) {
            addCriterion("WHOUSEID not between", value1, value2, "whouseid");
            return (Criteria) this;
        }

        public Criteria andWhousenameIsNull() {
            addCriterion("WHOUSENAME is null");
            return (Criteria) this;
        }

        public Criteria andWhousenameIsNotNull() {
            addCriterion("WHOUSENAME is not null");
            return (Criteria) this;
        }

        public Criteria andWhousenameEqualTo(String value) {
            addCriterion("WHOUSENAME =", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameNotEqualTo(String value) {
            addCriterion("WHOUSENAME <>", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameGreaterThan(String value) {
            addCriterion("WHOUSENAME >", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameGreaterThanOrEqualTo(String value) {
            addCriterion("WHOUSENAME >=", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameLessThan(String value) {
            addCriterion("WHOUSENAME <", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameLessThanOrEqualTo(String value) {
            addCriterion("WHOUSENAME <=", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameLike(String value) {
            addCriterion("WHOUSENAME like", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameNotLike(String value) {
            addCriterion("WHOUSENAME not like", value, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameIn(List<String> values) {
            addCriterion("WHOUSENAME in", values, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameNotIn(List<String> values) {
            addCriterion("WHOUSENAME not in", values, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameBetween(String value1, String value2) {
            addCriterion("WHOUSENAME between", value1, value2, "whousename");
            return (Criteria) this;
        }

        public Criteria andWhousenameNotBetween(String value1, String value2) {
            addCriterion("WHOUSENAME not between", value1, value2, "whousename");
            return (Criteria) this;
        }

        public Criteria andIntypeIsNull() {
            addCriterion("INTYPE is null");
            return (Criteria) this;
        }

        public Criteria andIntypeIsNotNull() {
            addCriterion("INTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIntypeEqualTo(String value) {
            addCriterion("INTYPE =", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeNotEqualTo(String value) {
            addCriterion("INTYPE <>", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeGreaterThan(String value) {
            addCriterion("INTYPE >", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeGreaterThanOrEqualTo(String value) {
            addCriterion("INTYPE >=", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeLessThan(String value) {
            addCriterion("INTYPE <", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeLessThanOrEqualTo(String value) {
            addCriterion("INTYPE <=", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeLike(String value) {
            addCriterion("INTYPE like", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeNotLike(String value) {
            addCriterion("INTYPE not like", value, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeIn(List<String> values) {
            addCriterion("INTYPE in", values, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeNotIn(List<String> values) {
            addCriterion("INTYPE not in", values, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeBetween(String value1, String value2) {
            addCriterion("INTYPE between", value1, value2, "intype");
            return (Criteria) this;
        }

        public Criteria andIntypeNotBetween(String value1, String value2) {
            addCriterion("INTYPE not between", value1, value2, "intype");
            return (Criteria) this;
        }

        public Criteria andRsheetnumIsNull() {
            addCriterion("RSHEETNUM is null");
            return (Criteria) this;
        }

        public Criteria andRsheetnumIsNotNull() {
            addCriterion("RSHEETNUM is not null");
            return (Criteria) this;
        }

        public Criteria andRsheetnumEqualTo(Integer value) {
            addCriterion("RSHEETNUM =", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumNotEqualTo(Integer value) {
            addCriterion("RSHEETNUM <>", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumGreaterThan(Integer value) {
            addCriterion("RSHEETNUM >", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("RSHEETNUM >=", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumLessThan(Integer value) {
            addCriterion("RSHEETNUM <", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumLessThanOrEqualTo(Integer value) {
            addCriterion("RSHEETNUM <=", value, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumIn(List<Integer> values) {
            addCriterion("RSHEETNUM in", values, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumNotIn(List<Integer> values) {
            addCriterion("RSHEETNUM not in", values, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumBetween(Integer value1, Integer value2) {
            addCriterion("RSHEETNUM between", value1, value2, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetnumNotBetween(Integer value1, Integer value2) {
            addCriterion("RSHEETNUM not between", value1, value2, "rsheetnum");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesIsNull() {
            addCriterion("RSHEETSDES is null");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesIsNotNull() {
            addCriterion("RSHEETSDES is not null");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesEqualTo(String value) {
            addCriterion("RSHEETSDES =", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesNotEqualTo(String value) {
            addCriterion("RSHEETSDES <>", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesGreaterThan(String value) {
            addCriterion("RSHEETSDES >", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesGreaterThanOrEqualTo(String value) {
            addCriterion("RSHEETSDES >=", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesLessThan(String value) {
            addCriterion("RSHEETSDES <", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesLessThanOrEqualTo(String value) {
            addCriterion("RSHEETSDES <=", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesLike(String value) {
            addCriterion("RSHEETSDES like", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesNotLike(String value) {
            addCriterion("RSHEETSDES not like", value, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesIn(List<String> values) {
            addCriterion("RSHEETSDES in", values, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesNotIn(List<String> values) {
            addCriterion("RSHEETSDES not in", values, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesBetween(String value1, String value2) {
            addCriterion("RSHEETSDES between", value1, value2, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andRsheetsdesNotBetween(String value1, String value2) {
            addCriterion("RSHEETSDES not between", value1, value2, "rsheetsdes");
            return (Criteria) this;
        }

        public Criteria andUnitidIsNull() {
            addCriterion("UNITID is null");
            return (Criteria) this;
        }

        public Criteria andUnitidIsNotNull() {
            addCriterion("UNITID is not null");
            return (Criteria) this;
        }

        public Criteria andUnitidEqualTo(String value) {
            addCriterion("UNITID =", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidNotEqualTo(String value) {
            addCriterion("UNITID <>", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidGreaterThan(String value) {
            addCriterion("UNITID >", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidGreaterThanOrEqualTo(String value) {
            addCriterion("UNITID >=", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidLessThan(String value) {
            addCriterion("UNITID <", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidLessThanOrEqualTo(String value) {
            addCriterion("UNITID <=", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidLike(String value) {
            addCriterion("UNITID like", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidNotLike(String value) {
            addCriterion("UNITID not like", value, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidIn(List<String> values) {
            addCriterion("UNITID in", values, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidNotIn(List<String> values) {
            addCriterion("UNITID not in", values, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidBetween(String value1, String value2) {
            addCriterion("UNITID between", value1, value2, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitidNotBetween(String value1, String value2) {
            addCriterion("UNITID not between", value1, value2, "unitid");
            return (Criteria) this;
        }

        public Criteria andUnitnameIsNull() {
            addCriterion("UNITNAME is null");
            return (Criteria) this;
        }

        public Criteria andUnitnameIsNotNull() {
            addCriterion("UNITNAME is not null");
            return (Criteria) this;
        }

        public Criteria andUnitnameEqualTo(String value) {
            addCriterion("UNITNAME =", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameNotEqualTo(String value) {
            addCriterion("UNITNAME <>", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameGreaterThan(String value) {
            addCriterion("UNITNAME >", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameGreaterThanOrEqualTo(String value) {
            addCriterion("UNITNAME >=", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameLessThan(String value) {
            addCriterion("UNITNAME <", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameLessThanOrEqualTo(String value) {
            addCriterion("UNITNAME <=", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameLike(String value) {
            addCriterion("UNITNAME like", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameNotLike(String value) {
            addCriterion("UNITNAME not like", value, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameIn(List<String> values) {
            addCriterion("UNITNAME in", values, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameNotIn(List<String> values) {
            addCriterion("UNITNAME not in", values, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameBetween(String value1, String value2) {
            addCriterion("UNITNAME between", value1, value2, "unitname");
            return (Criteria) this;
        }

        public Criteria andUnitnameNotBetween(String value1, String value2) {
            addCriterion("UNITNAME not between", value1, value2, "unitname");
            return (Criteria) this;
        }

        public Criteria andSheetmanIsNull() {
            addCriterion("SHEETMAN is null");
            return (Criteria) this;
        }

        public Criteria andSheetmanIsNotNull() {
            addCriterion("SHEETMAN is not null");
            return (Criteria) this;
        }

        public Criteria andSheetmanEqualTo(String value) {
            addCriterion("SHEETMAN =", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanNotEqualTo(String value) {
            addCriterion("SHEETMAN <>", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanGreaterThan(String value) {
            addCriterion("SHEETMAN >", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanGreaterThanOrEqualTo(String value) {
            addCriterion("SHEETMAN >=", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanLessThan(String value) {
            addCriterion("SHEETMAN <", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanLessThanOrEqualTo(String value) {
            addCriterion("SHEETMAN <=", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanLike(String value) {
            addCriterion("SHEETMAN like", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanNotLike(String value) {
            addCriterion("SHEETMAN not like", value, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanIn(List<String> values) {
            addCriterion("SHEETMAN in", values, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanNotIn(List<String> values) {
            addCriterion("SHEETMAN not in", values, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanBetween(String value1, String value2) {
            addCriterion("SHEETMAN between", value1, value2, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmanNotBetween(String value1, String value2) {
            addCriterion("SHEETMAN not between", value1, value2, "sheetman");
            return (Criteria) this;
        }

        public Criteria andSheetmnameIsNull() {
            addCriterion("SHEETMNAME is null");
            return (Criteria) this;
        }

        public Criteria andSheetmnameIsNotNull() {
            addCriterion("SHEETMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andSheetmnameEqualTo(String value) {
            addCriterion("SHEETMNAME =", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameNotEqualTo(String value) {
            addCriterion("SHEETMNAME <>", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameGreaterThan(String value) {
            addCriterion("SHEETMNAME >", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameGreaterThanOrEqualTo(String value) {
            addCriterion("SHEETMNAME >=", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameLessThan(String value) {
            addCriterion("SHEETMNAME <", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameLessThanOrEqualTo(String value) {
            addCriterion("SHEETMNAME <=", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameLike(String value) {
            addCriterion("SHEETMNAME like", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameNotLike(String value) {
            addCriterion("SHEETMNAME not like", value, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameIn(List<String> values) {
            addCriterion("SHEETMNAME in", values, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameNotIn(List<String> values) {
            addCriterion("SHEETMNAME not in", values, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameBetween(String value1, String value2) {
            addCriterion("SHEETMNAME between", value1, value2, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetmnameNotBetween(String value1, String value2) {
            addCriterion("SHEETMNAME not between", value1, value2, "sheetmname");
            return (Criteria) this;
        }

        public Criteria andSheetdateIsNull() {
            addCriterion("SHEETDATE is null");
            return (Criteria) this;
        }

        public Criteria andSheetdateIsNotNull() {
            addCriterion("SHEETDATE is not null");
            return (Criteria) this;
        }

        public Criteria andSheetdateEqualTo(Date value) {
            addCriterion("SHEETDATE =", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateNotEqualTo(Date value) {
            addCriterion("SHEETDATE <>", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateGreaterThan(Date value) {
            addCriterion("SHEETDATE >", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateGreaterThanOrEqualTo(Date value) {
            addCriterion("SHEETDATE >=", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateLessThan(Date value) {
            addCriterion("SHEETDATE <", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateLessThanOrEqualTo(Date value) {
            addCriterion("SHEETDATE <=", value, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateIn(List<Date> values) {
            addCriterion("SHEETDATE in", values, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateNotIn(List<Date> values) {
            addCriterion("SHEETDATE not in", values, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateBetween(Date value1, Date value2) {
            addCriterion("SHEETDATE between", value1, value2, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andSheetdateNotBetween(Date value1, Date value2) {
            addCriterion("SHEETDATE not between", value1, value2, "sheetdate");
            return (Criteria) this;
        }

        public Criteria andCheckmanIsNull() {
            addCriterion("CHECKMAN is null");
            return (Criteria) this;
        }

        public Criteria andCheckmanIsNotNull() {
            addCriterion("CHECKMAN is not null");
            return (Criteria) this;
        }

        public Criteria andCheckmanEqualTo(String value) {
            addCriterion("CHECKMAN =", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanNotEqualTo(String value) {
            addCriterion("CHECKMAN <>", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanGreaterThan(String value) {
            addCriterion("CHECKMAN >", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanGreaterThanOrEqualTo(String value) {
            addCriterion("CHECKMAN >=", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanLessThan(String value) {
            addCriterion("CHECKMAN <", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanLessThanOrEqualTo(String value) {
            addCriterion("CHECKMAN <=", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanLike(String value) {
            addCriterion("CHECKMAN like", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanNotLike(String value) {
            addCriterion("CHECKMAN not like", value, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanIn(List<String> values) {
            addCriterion("CHECKMAN in", values, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanNotIn(List<String> values) {
            addCriterion("CHECKMAN not in", values, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanBetween(String value1, String value2) {
            addCriterion("CHECKMAN between", value1, value2, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmanNotBetween(String value1, String value2) {
            addCriterion("CHECKMAN not between", value1, value2, "checkman");
            return (Criteria) this;
        }

        public Criteria andCheckmnameIsNull() {
            addCriterion("CHECKMNAME is null");
            return (Criteria) this;
        }

        public Criteria andCheckmnameIsNotNull() {
            addCriterion("CHECKMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andCheckmnameEqualTo(String value) {
            addCriterion("CHECKMNAME =", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameNotEqualTo(String value) {
            addCriterion("CHECKMNAME <>", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameGreaterThan(String value) {
            addCriterion("CHECKMNAME >", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameGreaterThanOrEqualTo(String value) {
            addCriterion("CHECKMNAME >=", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameLessThan(String value) {
            addCriterion("CHECKMNAME <", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameLessThanOrEqualTo(String value) {
            addCriterion("CHECKMNAME <=", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameLike(String value) {
            addCriterion("CHECKMNAME like", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameNotLike(String value) {
            addCriterion("CHECKMNAME not like", value, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameIn(List<String> values) {
            addCriterion("CHECKMNAME in", values, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameNotIn(List<String> values) {
            addCriterion("CHECKMNAME not in", values, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameBetween(String value1, String value2) {
            addCriterion("CHECKMNAME between", value1, value2, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckmnameNotBetween(String value1, String value2) {
            addCriterion("CHECKMNAME not between", value1, value2, "checkmname");
            return (Criteria) this;
        }

        public Criteria andCheckdateIsNull() {
            addCriterion("CHECKDATE is null");
            return (Criteria) this;
        }

        public Criteria andCheckdateIsNotNull() {
            addCriterion("CHECKDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCheckdateEqualTo(Date value) {
            addCriterion("CHECKDATE =", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateNotEqualTo(Date value) {
            addCriterion("CHECKDATE <>", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateGreaterThan(Date value) {
            addCriterion("CHECKDATE >", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateGreaterThanOrEqualTo(Date value) {
            addCriterion("CHECKDATE >=", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateLessThan(Date value) {
            addCriterion("CHECKDATE <", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateLessThanOrEqualTo(Date value) {
            addCriterion("CHECKDATE <=", value, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateIn(List<Date> values) {
            addCriterion("CHECKDATE in", values, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateNotIn(List<Date> values) {
            addCriterion("CHECKDATE not in", values, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateBetween(Date value1, Date value2) {
            addCriterion("CHECKDATE between", value1, value2, "checkdate");
            return (Criteria) this;
        }

        public Criteria andCheckdateNotBetween(Date value1, Date value2) {
            addCriterion("CHECKDATE not between", value1, value2, "checkdate");
            return (Criteria) this;
        }

        public Criteria andRegmanIsNull() {
            addCriterion("REGMAN is null");
            return (Criteria) this;
        }

        public Criteria andRegmanIsNotNull() {
            addCriterion("REGMAN is not null");
            return (Criteria) this;
        }

        public Criteria andRegmanEqualTo(String value) {
            addCriterion("REGMAN =", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanNotEqualTo(String value) {
            addCriterion("REGMAN <>", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanGreaterThan(String value) {
            addCriterion("REGMAN >", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanGreaterThanOrEqualTo(String value) {
            addCriterion("REGMAN >=", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanLessThan(String value) {
            addCriterion("REGMAN <", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanLessThanOrEqualTo(String value) {
            addCriterion("REGMAN <=", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanLike(String value) {
            addCriterion("REGMAN like", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanNotLike(String value) {
            addCriterion("REGMAN not like", value, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanIn(List<String> values) {
            addCriterion("REGMAN in", values, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanNotIn(List<String> values) {
            addCriterion("REGMAN not in", values, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanBetween(String value1, String value2) {
            addCriterion("REGMAN between", value1, value2, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmanNotBetween(String value1, String value2) {
            addCriterion("REGMAN not between", value1, value2, "regman");
            return (Criteria) this;
        }

        public Criteria andRegmnameIsNull() {
            addCriterion("REGMNAME is null");
            return (Criteria) this;
        }

        public Criteria andRegmnameIsNotNull() {
            addCriterion("REGMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRegmnameEqualTo(String value) {
            addCriterion("REGMNAME =", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameNotEqualTo(String value) {
            addCriterion("REGMNAME <>", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameGreaterThan(String value) {
            addCriterion("REGMNAME >", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameGreaterThanOrEqualTo(String value) {
            addCriterion("REGMNAME >=", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameLessThan(String value) {
            addCriterion("REGMNAME <", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameLessThanOrEqualTo(String value) {
            addCriterion("REGMNAME <=", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameLike(String value) {
            addCriterion("REGMNAME like", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameNotLike(String value) {
            addCriterion("REGMNAME not like", value, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameIn(List<String> values) {
            addCriterion("REGMNAME in", values, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameNotIn(List<String> values) {
            addCriterion("REGMNAME not in", values, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameBetween(String value1, String value2) {
            addCriterion("REGMNAME between", value1, value2, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegmnameNotBetween(String value1, String value2) {
            addCriterion("REGMNAME not between", value1, value2, "regmname");
            return (Criteria) this;
        }

        public Criteria andRegdateIsNull() {
            addCriterion("REGDATE is null");
            return (Criteria) this;
        }

        public Criteria andRegdateIsNotNull() {
            addCriterion("REGDATE is not null");
            return (Criteria) this;
        }

        public Criteria andRegdateEqualTo(Date value) {
            addCriterion("REGDATE =", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateNotEqualTo(Date value) {
            addCriterion("REGDATE <>", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateGreaterThan(Date value) {
            addCriterion("REGDATE >", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateGreaterThanOrEqualTo(Date value) {
            addCriterion("REGDATE >=", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateLessThan(Date value) {
            addCriterion("REGDATE <", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateLessThanOrEqualTo(Date value) {
            addCriterion("REGDATE <=", value, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateIn(List<Date> values) {
            addCriterion("REGDATE in", values, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateNotIn(List<Date> values) {
            addCriterion("REGDATE not in", values, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateBetween(Date value1, Date value2) {
            addCriterion("REGDATE between", value1, value2, "regdate");
            return (Criteria) this;
        }

        public Criteria andRegdateNotBetween(Date value1, Date value2) {
            addCriterion("REGDATE not between", value1, value2, "regdate");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoIsNull() {
            addCriterion("PAYSHEETNO is null");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoIsNotNull() {
            addCriterion("PAYSHEETNO is not null");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoEqualTo(String value) {
            addCriterion("PAYSHEETNO =", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoNotEqualTo(String value) {
            addCriterion("PAYSHEETNO <>", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoGreaterThan(String value) {
            addCriterion("PAYSHEETNO >", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoGreaterThanOrEqualTo(String value) {
            addCriterion("PAYSHEETNO >=", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoLessThan(String value) {
            addCriterion("PAYSHEETNO <", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoLessThanOrEqualTo(String value) {
            addCriterion("PAYSHEETNO <=", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoLike(String value) {
            addCriterion("PAYSHEETNO like", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoNotLike(String value) {
            addCriterion("PAYSHEETNO not like", value, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoIn(List<String> values) {
            addCriterion("PAYSHEETNO in", values, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoNotIn(List<String> values) {
            addCriterion("PAYSHEETNO not in", values, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoBetween(String value1, String value2) {
            addCriterion("PAYSHEETNO between", value1, value2, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaysheetnoNotBetween(String value1, String value2) {
            addCriterion("PAYSHEETNO not between", value1, value2, "paysheetno");
            return (Criteria) this;
        }

        public Criteria andPaymanIsNull() {
            addCriterion("PAYMAN is null");
            return (Criteria) this;
        }

        public Criteria andPaymanIsNotNull() {
            addCriterion("PAYMAN is not null");
            return (Criteria) this;
        }

        public Criteria andPaymanEqualTo(String value) {
            addCriterion("PAYMAN =", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanNotEqualTo(String value) {
            addCriterion("PAYMAN <>", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanGreaterThan(String value) {
            addCriterion("PAYMAN >", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMAN >=", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanLessThan(String value) {
            addCriterion("PAYMAN <", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanLessThanOrEqualTo(String value) {
            addCriterion("PAYMAN <=", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanLike(String value) {
            addCriterion("PAYMAN like", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanNotLike(String value) {
            addCriterion("PAYMAN not like", value, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanIn(List<String> values) {
            addCriterion("PAYMAN in", values, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanNotIn(List<String> values) {
            addCriterion("PAYMAN not in", values, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanBetween(String value1, String value2) {
            addCriterion("PAYMAN between", value1, value2, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymanNotBetween(String value1, String value2) {
            addCriterion("PAYMAN not between", value1, value2, "payman");
            return (Criteria) this;
        }

        public Criteria andPaymnameIsNull() {
            addCriterion("PAYMNAME is null");
            return (Criteria) this;
        }

        public Criteria andPaymnameIsNotNull() {
            addCriterion("PAYMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPaymnameEqualTo(String value) {
            addCriterion("PAYMNAME =", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameNotEqualTo(String value) {
            addCriterion("PAYMNAME <>", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameGreaterThan(String value) {
            addCriterion("PAYMNAME >", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMNAME >=", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameLessThan(String value) {
            addCriterion("PAYMNAME <", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameLessThanOrEqualTo(String value) {
            addCriterion("PAYMNAME <=", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameLike(String value) {
            addCriterion("PAYMNAME like", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameNotLike(String value) {
            addCriterion("PAYMNAME not like", value, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameIn(List<String> values) {
            addCriterion("PAYMNAME in", values, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameNotIn(List<String> values) {
            addCriterion("PAYMNAME not in", values, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameBetween(String value1, String value2) {
            addCriterion("PAYMNAME between", value1, value2, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaymnameNotBetween(String value1, String value2) {
            addCriterion("PAYMNAME not between", value1, value2, "paymname");
            return (Criteria) this;
        }

        public Criteria andPaydateIsNull() {
            addCriterion("PAYDATE is null");
            return (Criteria) this;
        }

        public Criteria andPaydateIsNotNull() {
            addCriterion("PAYDATE is not null");
            return (Criteria) this;
        }

        public Criteria andPaydateEqualTo(Date value) {
            addCriterion("PAYDATE =", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateNotEqualTo(Date value) {
            addCriterion("PAYDATE <>", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateGreaterThan(Date value) {
            addCriterion("PAYDATE >", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateGreaterThanOrEqualTo(Date value) {
            addCriterion("PAYDATE >=", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateLessThan(Date value) {
            addCriterion("PAYDATE <", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateLessThanOrEqualTo(Date value) {
            addCriterion("PAYDATE <=", value, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateIn(List<Date> values) {
            addCriterion("PAYDATE in", values, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateNotIn(List<Date> values) {
            addCriterion("PAYDATE not in", values, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateBetween(Date value1, Date value2) {
            addCriterion("PAYDATE between", value1, value2, "paydate");
            return (Criteria) this;
        }

        public Criteria andPaydateNotBetween(Date value1, Date value2) {
            addCriterion("PAYDATE not between", value1, value2, "paydate");
            return (Criteria) this;
        }

        public Criteria andCancelflagIsNull() {
            addCriterion("CANCELFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCancelflagIsNotNull() {
            addCriterion("CANCELFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCancelflagEqualTo(String value) {
            addCriterion("CANCELFLAG =", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagNotEqualTo(String value) {
            addCriterion("CANCELFLAG <>", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagGreaterThan(String value) {
            addCriterion("CANCELFLAG >", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagGreaterThanOrEqualTo(String value) {
            addCriterion("CANCELFLAG >=", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagLessThan(String value) {
            addCriterion("CANCELFLAG <", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagLessThanOrEqualTo(String value) {
            addCriterion("CANCELFLAG <=", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagLike(String value) {
            addCriterion("CANCELFLAG like", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagNotLike(String value) {
            addCriterion("CANCELFLAG not like", value, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagIn(List<String> values) {
            addCriterion("CANCELFLAG in", values, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagNotIn(List<String> values) {
            addCriterion("CANCELFLAG not in", values, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagBetween(String value1, String value2) {
            addCriterion("CANCELFLAG between", value1, value2, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCancelflagNotBetween(String value1, String value2) {
            addCriterion("CANCELFLAG not between", value1, value2, "cancelflag");
            return (Criteria) this;
        }

        public Criteria andCanceldesIsNull() {
            addCriterion("CANCELDES is null");
            return (Criteria) this;
        }

        public Criteria andCanceldesIsNotNull() {
            addCriterion("CANCELDES is not null");
            return (Criteria) this;
        }

        public Criteria andCanceldesEqualTo(String value) {
            addCriterion("CANCELDES =", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesNotEqualTo(String value) {
            addCriterion("CANCELDES <>", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesGreaterThan(String value) {
            addCriterion("CANCELDES >", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesGreaterThanOrEqualTo(String value) {
            addCriterion("CANCELDES >=", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesLessThan(String value) {
            addCriterion("CANCELDES <", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesLessThanOrEqualTo(String value) {
            addCriterion("CANCELDES <=", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesLike(String value) {
            addCriterion("CANCELDES like", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesNotLike(String value) {
            addCriterion("CANCELDES not like", value, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesIn(List<String> values) {
            addCriterion("CANCELDES in", values, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesNotIn(List<String> values) {
            addCriterion("CANCELDES not in", values, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesBetween(String value1, String value2) {
            addCriterion("CANCELDES between", value1, value2, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCanceldesNotBetween(String value1, String value2) {
            addCriterion("CANCELDES not between", value1, value2, "canceldes");
            return (Criteria) this;
        }

        public Criteria andCommentsIsNull() {
            addCriterion("COMMENTS is null");
            return (Criteria) this;
        }

        public Criteria andCommentsIsNotNull() {
            addCriterion("COMMENTS is not null");
            return (Criteria) this;
        }

        public Criteria andCommentsEqualTo(String value) {
            addCriterion("COMMENTS =", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotEqualTo(String value) {
            addCriterion("COMMENTS <>", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsGreaterThan(String value) {
            addCriterion("COMMENTS >", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsGreaterThanOrEqualTo(String value) {
            addCriterion("COMMENTS >=", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLessThan(String value) {
            addCriterion("COMMENTS <", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLessThanOrEqualTo(String value) {
            addCriterion("COMMENTS <=", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsLike(String value) {
            addCriterion("COMMENTS like", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotLike(String value) {
            addCriterion("COMMENTS not like", value, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsIn(List<String> values) {
            addCriterion("COMMENTS in", values, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotIn(List<String> values) {
            addCriterion("COMMENTS not in", values, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsBetween(String value1, String value2) {
            addCriterion("COMMENTS between", value1, value2, "comments");
            return (Criteria) this;
        }

        public Criteria andCommentsNotBetween(String value1, String value2) {
            addCriterion("COMMENTS not between", value1, value2, "comments");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitIsNull() {
            addCriterion("APPLYBUYUNIT is null");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitIsNotNull() {
            addCriterion("APPLYBUYUNIT is not null");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitEqualTo(String value) {
            addCriterion("APPLYBUYUNIT =", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitNotEqualTo(String value) {
            addCriterion("APPLYBUYUNIT <>", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitGreaterThan(String value) {
            addCriterion("APPLYBUYUNIT >", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitGreaterThanOrEqualTo(String value) {
            addCriterion("APPLYBUYUNIT >=", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitLessThan(String value) {
            addCriterion("APPLYBUYUNIT <", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitLessThanOrEqualTo(String value) {
            addCriterion("APPLYBUYUNIT <=", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitLike(String value) {
            addCriterion("APPLYBUYUNIT like", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitNotLike(String value) {
            addCriterion("APPLYBUYUNIT not like", value, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitIn(List<String> values) {
            addCriterion("APPLYBUYUNIT in", values, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitNotIn(List<String> values) {
            addCriterion("APPLYBUYUNIT not in", values, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitBetween(String value1, String value2) {
            addCriterion("APPLYBUYUNIT between", value1, value2, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuyunitNotBetween(String value1, String value2) {
            addCriterion("APPLYBUYUNIT not between", value1, value2, "applybuyunit");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidIsNull() {
            addCriterion("APPLYBUYSHEETID is null");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidIsNotNull() {
            addCriterion("APPLYBUYSHEETID is not null");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidEqualTo(String value) {
            addCriterion("APPLYBUYSHEETID =", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidNotEqualTo(String value) {
            addCriterion("APPLYBUYSHEETID <>", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidGreaterThan(String value) {
            addCriterion("APPLYBUYSHEETID >", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidGreaterThanOrEqualTo(String value) {
            addCriterion("APPLYBUYSHEETID >=", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidLessThan(String value) {
            addCriterion("APPLYBUYSHEETID <", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidLessThanOrEqualTo(String value) {
            addCriterion("APPLYBUYSHEETID <=", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidLike(String value) {
            addCriterion("APPLYBUYSHEETID like", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidNotLike(String value) {
            addCriterion("APPLYBUYSHEETID not like", value, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidIn(List<String> values) {
            addCriterion("APPLYBUYSHEETID in", values, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidNotIn(List<String> values) {
            addCriterion("APPLYBUYSHEETID not in", values, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidBetween(String value1, String value2) {
            addCriterion("APPLYBUYSHEETID between", value1, value2, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andApplybuysheetidNotBetween(String value1, String value2) {
            addCriterion("APPLYBUYSHEETID not between", value1, value2, "applybuysheetid");
            return (Criteria) this;
        }

        public Criteria andChgdateIsNull() {
            addCriterion("CHGDATE is null");
            return (Criteria) this;
        }

        public Criteria andChgdateIsNotNull() {
            addCriterion("CHGDATE is not null");
            return (Criteria) this;
        }

        public Criteria andChgdateEqualTo(Date value) {
            addCriterion("CHGDATE =", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateNotEqualTo(Date value) {
            addCriterion("CHGDATE <>", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateGreaterThan(Date value) {
            addCriterion("CHGDATE >", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateGreaterThanOrEqualTo(Date value) {
            addCriterion("CHGDATE >=", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateLessThan(Date value) {
            addCriterion("CHGDATE <", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateLessThanOrEqualTo(Date value) {
            addCriterion("CHGDATE <=", value, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateIn(List<Date> values) {
            addCriterion("CHGDATE in", values, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateNotIn(List<Date> values) {
            addCriterion("CHGDATE not in", values, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateBetween(Date value1, Date value2) {
            addCriterion("CHGDATE between", value1, value2, "chgdate");
            return (Criteria) this;
        }

        public Criteria andChgdateNotBetween(Date value1, Date value2) {
            addCriterion("CHGDATE not between", value1, value2, "chgdate");
            return (Criteria) this;
        }

        public Criteria andDocforredIsNull() {
            addCriterion("DOCFORRED is null");
            return (Criteria) this;
        }

        public Criteria andDocforredIsNotNull() {
            addCriterion("DOCFORRED is not null");
            return (Criteria) this;
        }

        public Criteria andDocforredEqualTo(String value) {
            addCriterion("DOCFORRED =", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredNotEqualTo(String value) {
            addCriterion("DOCFORRED <>", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredGreaterThan(String value) {
            addCriterion("DOCFORRED >", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredGreaterThanOrEqualTo(String value) {
            addCriterion("DOCFORRED >=", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredLessThan(String value) {
            addCriterion("DOCFORRED <", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredLessThanOrEqualTo(String value) {
            addCriterion("DOCFORRED <=", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredLike(String value) {
            addCriterion("DOCFORRED like", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredNotLike(String value) {
            addCriterion("DOCFORRED not like", value, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredIn(List<String> values) {
            addCriterion("DOCFORRED in", values, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredNotIn(List<String> values) {
            addCriterion("DOCFORRED not in", values, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredBetween(String value1, String value2) {
            addCriterion("DOCFORRED between", value1, value2, "docforred");
            return (Criteria) this;
        }

        public Criteria andDocforredNotBetween(String value1, String value2) {
            addCriterion("DOCFORRED not between", value1, value2, "docforred");
            return (Criteria) this;
        }

        public Criteria andReceiveidIsNull() {
            addCriterion("RECEIVEID is null");
            return (Criteria) this;
        }

        public Criteria andReceiveidIsNotNull() {
            addCriterion("RECEIVEID is not null");
            return (Criteria) this;
        }

        public Criteria andReceiveidEqualTo(String value) {
            addCriterion("RECEIVEID =", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidNotEqualTo(String value) {
            addCriterion("RECEIVEID <>", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidGreaterThan(String value) {
            addCriterion("RECEIVEID >", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidGreaterThanOrEqualTo(String value) {
            addCriterion("RECEIVEID >=", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidLessThan(String value) {
            addCriterion("RECEIVEID <", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidLessThanOrEqualTo(String value) {
            addCriterion("RECEIVEID <=", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidLike(String value) {
            addCriterion("RECEIVEID like", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidNotLike(String value) {
            addCriterion("RECEIVEID not like", value, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidIn(List<String> values) {
            addCriterion("RECEIVEID in", values, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidNotIn(List<String> values) {
            addCriterion("RECEIVEID not in", values, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidBetween(String value1, String value2) {
            addCriterion("RECEIVEID between", value1, value2, "receiveid");
            return (Criteria) this;
        }

        public Criteria andReceiveidNotBetween(String value1, String value2) {
            addCriterion("RECEIVEID not between", value1, value2, "receiveid");
            return (Criteria) this;
        }

        public Criteria andOrdernoIsNull() {
            addCriterion("ORDERNO is null");
            return (Criteria) this;
        }

        public Criteria andOrdernoIsNotNull() {
            addCriterion("ORDERNO is not null");
            return (Criteria) this;
        }

        public Criteria andOrdernoEqualTo(String value) {
            addCriterion("ORDERNO =", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoNotEqualTo(String value) {
            addCriterion("ORDERNO <>", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoGreaterThan(String value) {
            addCriterion("ORDERNO >", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoGreaterThanOrEqualTo(String value) {
            addCriterion("ORDERNO >=", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoLessThan(String value) {
            addCriterion("ORDERNO <", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoLessThanOrEqualTo(String value) {
            addCriterion("ORDERNO <=", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoLike(String value) {
            addCriterion("ORDERNO like", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoNotLike(String value) {
            addCriterion("ORDERNO not like", value, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoIn(List<String> values) {
            addCriterion("ORDERNO in", values, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoNotIn(List<String> values) {
            addCriterion("ORDERNO not in", values, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoBetween(String value1, String value2) {
            addCriterion("ORDERNO between", value1, value2, "orderno");
            return (Criteria) this;
        }

        public Criteria andOrdernoNotBetween(String value1, String value2) {
            addCriterion("ORDERNO not between", value1, value2, "orderno");
            return (Criteria) this;
        }

        public Criteria andOutinfitidIsNull() {
            addCriterion("OUTINFITID is null");
            return (Criteria) this;
        }

        public Criteria andOutinfitidIsNotNull() {
            addCriterion("OUTINFITID is not null");
            return (Criteria) this;
        }

        public Criteria andOutinfitidEqualTo(String value) {
            addCriterion("OUTINFITID =", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidNotEqualTo(String value) {
            addCriterion("OUTINFITID <>", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidGreaterThan(String value) {
            addCriterion("OUTINFITID >", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidGreaterThanOrEqualTo(String value) {
            addCriterion("OUTINFITID >=", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidLessThan(String value) {
            addCriterion("OUTINFITID <", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidLessThanOrEqualTo(String value) {
            addCriterion("OUTINFITID <=", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidLike(String value) {
            addCriterion("OUTINFITID like", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidNotLike(String value) {
            addCriterion("OUTINFITID not like", value, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidIn(List<String> values) {
            addCriterion("OUTINFITID in", values, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidNotIn(List<String> values) {
            addCriterion("OUTINFITID not in", values, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidBetween(String value1, String value2) {
            addCriterion("OUTINFITID between", value1, value2, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andOutinfitidNotBetween(String value1, String value2) {
            addCriterion("OUTINFITID not between", value1, value2, "outinfitid");
            return (Criteria) this;
        }

        public Criteria andPurchasedateIsNull() {
            addCriterion("PURCHASEDATE is null");
            return (Criteria) this;
        }

        public Criteria andPurchasedateIsNotNull() {
            addCriterion("PURCHASEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasedateEqualTo(Date value) {
            addCriterion("PURCHASEDATE =", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateNotEqualTo(Date value) {
            addCriterion("PURCHASEDATE <>", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateGreaterThan(Date value) {
            addCriterion("PURCHASEDATE >", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateGreaterThanOrEqualTo(Date value) {
            addCriterion("PURCHASEDATE >=", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateLessThan(Date value) {
            addCriterion("PURCHASEDATE <", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateLessThanOrEqualTo(Date value) {
            addCriterion("PURCHASEDATE <=", value, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateIn(List<Date> values) {
            addCriterion("PURCHASEDATE in", values, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateNotIn(List<Date> values) {
            addCriterion("PURCHASEDATE not in", values, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateBetween(Date value1, Date value2) {
            addCriterion("PURCHASEDATE between", value1, value2, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andPurchasedateNotBetween(Date value1, Date value2) {
            addCriterion("PURCHASEDATE not between", value1, value2, "purchasedate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoIsNull() {
            addCriterion("INVOICE_NO is null");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoIsNotNull() {
            addCriterion("INVOICE_NO is not null");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoEqualTo(String value) {
            addCriterion("INVOICE_NO =", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoNotEqualTo(String value) {
            addCriterion("INVOICE_NO <>", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoGreaterThan(String value) {
            addCriterion("INVOICE_NO >", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoGreaterThanOrEqualTo(String value) {
            addCriterion("INVOICE_NO >=", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoLessThan(String value) {
            addCriterion("INVOICE_NO <", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoLessThanOrEqualTo(String value) {
            addCriterion("INVOICE_NO <=", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoLike(String value) {
            addCriterion("INVOICE_NO like", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoNotLike(String value) {
            addCriterion("INVOICE_NO not like", value, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoIn(List<String> values) {
            addCriterion("INVOICE_NO in", values, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoNotIn(List<String> values) {
            addCriterion("INVOICE_NO not in", values, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoBetween(String value1, String value2) {
            addCriterion("INVOICE_NO between", value1, value2, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andInvoiceNoNotBetween(String value1, String value2) {
            addCriterion("INVOICE_NO not between", value1, value2, "invoiceNo");
            return (Criteria) this;
        }

        public Criteria andTracelogIsNull() {
            addCriterion("TRACELOG is null");
            return (Criteria) this;
        }

        public Criteria andTracelogIsNotNull() {
            addCriterion("TRACELOG is not null");
            return (Criteria) this;
        }

        public Criteria andTracelogEqualTo(Date value) {
            addCriterion("TRACELOG =", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogNotEqualTo(Date value) {
            addCriterion("TRACELOG <>", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogGreaterThan(Date value) {
            addCriterion("TRACELOG >", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogGreaterThanOrEqualTo(Date value) {
            addCriterion("TRACELOG >=", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogLessThan(Date value) {
            addCriterion("TRACELOG <", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogLessThanOrEqualTo(Date value) {
            addCriterion("TRACELOG <=", value, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogIn(List<Date> values) {
            addCriterion("TRACELOG in", values, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogNotIn(List<Date> values) {
            addCriterion("TRACELOG not in", values, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogBetween(Date value1, Date value2) {
            addCriterion("TRACELOG between", value1, value2, "tracelog");
            return (Criteria) this;
        }

        public Criteria andTracelogNotBetween(Date value1, Date value2) {
            addCriterion("TRACELOG not between", value1, value2, "tracelog");
            return (Criteria) this;
        }

        public Criteria andPurchasewayIsNull() {
            addCriterion("PURCHASEWAY is null");
            return (Criteria) this;
        }

        public Criteria andPurchasewayIsNotNull() {
            addCriterion("PURCHASEWAY is not null");
            return (Criteria) this;
        }

        public Criteria andPurchasewayEqualTo(String value) {
            addCriterion("PURCHASEWAY =", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayNotEqualTo(String value) {
            addCriterion("PURCHASEWAY <>", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayGreaterThan(String value) {
            addCriterion("PURCHASEWAY >", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayGreaterThanOrEqualTo(String value) {
            addCriterion("PURCHASEWAY >=", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayLessThan(String value) {
            addCriterion("PURCHASEWAY <", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayLessThanOrEqualTo(String value) {
            addCriterion("PURCHASEWAY <=", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayLike(String value) {
            addCriterion("PURCHASEWAY like", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayNotLike(String value) {
            addCriterion("PURCHASEWAY not like", value, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayIn(List<String> values) {
            addCriterion("PURCHASEWAY in", values, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayNotIn(List<String> values) {
            addCriterion("PURCHASEWAY not in", values, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayBetween(String value1, String value2) {
            addCriterion("PURCHASEWAY between", value1, value2, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andPurchasewayNotBetween(String value1, String value2) {
            addCriterion("PURCHASEWAY not between", value1, value2, "purchaseway");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeIsNull() {
            addCriterion("DOCFORREDTYPE is null");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeIsNotNull() {
            addCriterion("DOCFORREDTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeEqualTo(String value) {
            addCriterion("DOCFORREDTYPE =", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeNotEqualTo(String value) {
            addCriterion("DOCFORREDTYPE <>", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeGreaterThan(String value) {
            addCriterion("DOCFORREDTYPE >", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeGreaterThanOrEqualTo(String value) {
            addCriterion("DOCFORREDTYPE >=", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeLessThan(String value) {
            addCriterion("DOCFORREDTYPE <", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeLessThanOrEqualTo(String value) {
            addCriterion("DOCFORREDTYPE <=", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeLike(String value) {
            addCriterion("DOCFORREDTYPE like", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeNotLike(String value) {
            addCriterion("DOCFORREDTYPE not like", value, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeIn(List<String> values) {
            addCriterion("DOCFORREDTYPE in", values, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeNotIn(List<String> values) {
            addCriterion("DOCFORREDTYPE not in", values, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeBetween(String value1, String value2) {
            addCriterion("DOCFORREDTYPE between", value1, value2, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andDocforredtypeNotBetween(String value1, String value2) {
            addCriterion("DOCFORREDTYPE not between", value1, value2, "docforredtype");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusIsNull() {
            addCriterion("PAYMENTSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusIsNotNull() {
            addCriterion("PAYMENTSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusEqualTo(String value) {
            addCriterion("PAYMENTSTATUS =", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusNotEqualTo(String value) {
            addCriterion("PAYMENTSTATUS <>", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusGreaterThan(String value) {
            addCriterion("PAYMENTSTATUS >", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMENTSTATUS >=", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusLessThan(String value) {
            addCriterion("PAYMENTSTATUS <", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusLessThanOrEqualTo(String value) {
            addCriterion("PAYMENTSTATUS <=", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusLike(String value) {
            addCriterion("PAYMENTSTATUS like", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusNotLike(String value) {
            addCriterion("PAYMENTSTATUS not like", value, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusIn(List<String> values) {
            addCriterion("PAYMENTSTATUS in", values, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusNotIn(List<String> values) {
            addCriterion("PAYMENTSTATUS not in", values, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusBetween(String value1, String value2) {
            addCriterion("PAYMENTSTATUS between", value1, value2, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentstatusNotBetween(String value1, String value2) {
            addCriterion("PAYMENTSTATUS not between", value1, value2, "paymentstatus");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanIsNull() {
            addCriterion("PAYMENTCHECKMAN is null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanIsNotNull() {
            addCriterion("PAYMENTCHECKMAN is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanEqualTo(String value) {
            addCriterion("PAYMENTCHECKMAN =", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanNotEqualTo(String value) {
            addCriterion("PAYMENTCHECKMAN <>", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanGreaterThan(String value) {
            addCriterion("PAYMENTCHECKMAN >", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKMAN >=", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanLessThan(String value) {
            addCriterion("PAYMENTCHECKMAN <", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanLessThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKMAN <=", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanLike(String value) {
            addCriterion("PAYMENTCHECKMAN like", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanNotLike(String value) {
            addCriterion("PAYMENTCHECKMAN not like", value, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanIn(List<String> values) {
            addCriterion("PAYMENTCHECKMAN in", values, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanNotIn(List<String> values) {
            addCriterion("PAYMENTCHECKMAN not in", values, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKMAN between", value1, value2, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmanNotBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKMAN not between", value1, value2, "paymentcheckman");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameIsNull() {
            addCriterion("PAYMENTCHECKMNAME is null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameIsNotNull() {
            addCriterion("PAYMENTCHECKMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameEqualTo(String value) {
            addCriterion("PAYMENTCHECKMNAME =", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameNotEqualTo(String value) {
            addCriterion("PAYMENTCHECKMNAME <>", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameGreaterThan(String value) {
            addCriterion("PAYMENTCHECKMNAME >", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKMNAME >=", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameLessThan(String value) {
            addCriterion("PAYMENTCHECKMNAME <", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameLessThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKMNAME <=", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameLike(String value) {
            addCriterion("PAYMENTCHECKMNAME like", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameNotLike(String value) {
            addCriterion("PAYMENTCHECKMNAME not like", value, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameIn(List<String> values) {
            addCriterion("PAYMENTCHECKMNAME in", values, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameNotIn(List<String> values) {
            addCriterion("PAYMENTCHECKMNAME not in", values, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKMNAME between", value1, value2, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckmnameNotBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKMNAME not between", value1, value2, "paymentcheckmname");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateIsNull() {
            addCriterion("PAYMENTCHECKDATE is null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateIsNotNull() {
            addCriterion("PAYMENTCHECKDATE is not null");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateEqualTo(String value) {
            addCriterion("PAYMENTCHECKDATE =", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateNotEqualTo(String value) {
            addCriterion("PAYMENTCHECKDATE <>", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateGreaterThan(String value) {
            addCriterion("PAYMENTCHECKDATE >", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateGreaterThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKDATE >=", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateLessThan(String value) {
            addCriterion("PAYMENTCHECKDATE <", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateLessThanOrEqualTo(String value) {
            addCriterion("PAYMENTCHECKDATE <=", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateLike(String value) {
            addCriterion("PAYMENTCHECKDATE like", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateNotLike(String value) {
            addCriterion("PAYMENTCHECKDATE not like", value, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateIn(List<String> values) {
            addCriterion("PAYMENTCHECKDATE in", values, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateNotIn(List<String> values) {
            addCriterion("PAYMENTCHECKDATE not in", values, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKDATE between", value1, value2, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andPaymentcheckdateNotBetween(String value1, String value2) {
            addCriterion("PAYMENTCHECKDATE not between", value1, value2, "paymentcheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancestatusIsNull() {
            addCriterion("FINANCESTATUS is null");
            return (Criteria) this;
        }

        public Criteria andFinancestatusIsNotNull() {
            addCriterion("FINANCESTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andFinancestatusEqualTo(String value) {
            addCriterion("FINANCESTATUS =", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusNotEqualTo(String value) {
            addCriterion("FINANCESTATUS <>", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusGreaterThan(String value) {
            addCriterion("FINANCESTATUS >", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusGreaterThanOrEqualTo(String value) {
            addCriterion("FINANCESTATUS >=", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusLessThan(String value) {
            addCriterion("FINANCESTATUS <", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusLessThanOrEqualTo(String value) {
            addCriterion("FINANCESTATUS <=", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusLike(String value) {
            addCriterion("FINANCESTATUS like", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusNotLike(String value) {
            addCriterion("FINANCESTATUS not like", value, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusIn(List<String> values) {
            addCriterion("FINANCESTATUS in", values, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusNotIn(List<String> values) {
            addCriterion("FINANCESTATUS not in", values, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusBetween(String value1, String value2) {
            addCriterion("FINANCESTATUS between", value1, value2, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancestatusNotBetween(String value1, String value2) {
            addCriterion("FINANCESTATUS not between", value1, value2, "financestatus");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanIsNull() {
            addCriterion("FINANCECHECKMAN is null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanIsNotNull() {
            addCriterion("FINANCECHECKMAN is not null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanEqualTo(String value) {
            addCriterion("FINANCECHECKMAN =", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanNotEqualTo(String value) {
            addCriterion("FINANCECHECKMAN <>", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanGreaterThan(String value) {
            addCriterion("FINANCECHECKMAN >", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanGreaterThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKMAN >=", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanLessThan(String value) {
            addCriterion("FINANCECHECKMAN <", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanLessThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKMAN <=", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanLike(String value) {
            addCriterion("FINANCECHECKMAN like", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanNotLike(String value) {
            addCriterion("FINANCECHECKMAN not like", value, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanIn(List<String> values) {
            addCriterion("FINANCECHECKMAN in", values, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanNotIn(List<String> values) {
            addCriterion("FINANCECHECKMAN not in", values, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanBetween(String value1, String value2) {
            addCriterion("FINANCECHECKMAN between", value1, value2, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmanNotBetween(String value1, String value2) {
            addCriterion("FINANCECHECKMAN not between", value1, value2, "financecheckman");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameIsNull() {
            addCriterion("FINANCECHECKMNAME is null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameIsNotNull() {
            addCriterion("FINANCECHECKMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameEqualTo(String value) {
            addCriterion("FINANCECHECKMNAME =", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameNotEqualTo(String value) {
            addCriterion("FINANCECHECKMNAME <>", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameGreaterThan(String value) {
            addCriterion("FINANCECHECKMNAME >", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameGreaterThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKMNAME >=", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameLessThan(String value) {
            addCriterion("FINANCECHECKMNAME <", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameLessThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKMNAME <=", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameLike(String value) {
            addCriterion("FINANCECHECKMNAME like", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameNotLike(String value) {
            addCriterion("FINANCECHECKMNAME not like", value, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameIn(List<String> values) {
            addCriterion("FINANCECHECKMNAME in", values, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameNotIn(List<String> values) {
            addCriterion("FINANCECHECKMNAME not in", values, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameBetween(String value1, String value2) {
            addCriterion("FINANCECHECKMNAME between", value1, value2, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckmnameNotBetween(String value1, String value2) {
            addCriterion("FINANCECHECKMNAME not between", value1, value2, "financecheckmname");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateIsNull() {
            addCriterion("FINANCECHECKDATE is null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateIsNotNull() {
            addCriterion("FINANCECHECKDATE is not null");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateEqualTo(String value) {
            addCriterion("FINANCECHECKDATE =", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateNotEqualTo(String value) {
            addCriterion("FINANCECHECKDATE <>", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateGreaterThan(String value) {
            addCriterion("FINANCECHECKDATE >", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateGreaterThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKDATE >=", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateLessThan(String value) {
            addCriterion("FINANCECHECKDATE <", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateLessThanOrEqualTo(String value) {
            addCriterion("FINANCECHECKDATE <=", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateLike(String value) {
            addCriterion("FINANCECHECKDATE like", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateNotLike(String value) {
            addCriterion("FINANCECHECKDATE not like", value, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateIn(List<String> values) {
            addCriterion("FINANCECHECKDATE in", values, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateNotIn(List<String> values) {
            addCriterion("FINANCECHECKDATE not in", values, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateBetween(String value1, String value2) {
            addCriterion("FINANCECHECKDATE between", value1, value2, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andFinancecheckdateNotBetween(String value1, String value2) {
            addCriterion("FINANCECHECKDATE not between", value1, value2, "financecheckdate");
            return (Criteria) this;
        }

        public Criteria andShippingordernoIsNull() {
            addCriterion("SHIPPINGORDERNO is null");
            return (Criteria) this;
        }

        public Criteria andShippingordernoIsNotNull() {
            addCriterion("SHIPPINGORDERNO is not null");
            return (Criteria) this;
        }

        public Criteria andShippingordernoEqualTo(String value) {
            addCriterion("SHIPPINGORDERNO =", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoNotEqualTo(String value) {
            addCriterion("SHIPPINGORDERNO <>", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoGreaterThan(String value) {
            addCriterion("SHIPPINGORDERNO >", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoGreaterThanOrEqualTo(String value) {
            addCriterion("SHIPPINGORDERNO >=", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoLessThan(String value) {
            addCriterion("SHIPPINGORDERNO <", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoLessThanOrEqualTo(String value) {
            addCriterion("SHIPPINGORDERNO <=", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoLike(String value) {
            addCriterion("SHIPPINGORDERNO like", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoNotLike(String value) {
            addCriterion("SHIPPINGORDERNO not like", value, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoIn(List<String> values) {
            addCriterion("SHIPPINGORDERNO in", values, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoNotIn(List<String> values) {
            addCriterion("SHIPPINGORDERNO not in", values, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoBetween(String value1, String value2) {
            addCriterion("SHIPPINGORDERNO between", value1, value2, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andShippingordernoNotBetween(String value1, String value2) {
            addCriterion("SHIPPINGORDERNO not between", value1, value2, "shippingorderno");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagIsNull() {
            addCriterion("JCUPLOADFLAG is null");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagIsNotNull() {
            addCriterion("JCUPLOADFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagEqualTo(String value) {
            addCriterion("JCUPLOADFLAG =", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagNotEqualTo(String value) {
            addCriterion("JCUPLOADFLAG <>", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagGreaterThan(String value) {
            addCriterion("JCUPLOADFLAG >", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagGreaterThanOrEqualTo(String value) {
            addCriterion("JCUPLOADFLAG >=", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagLessThan(String value) {
            addCriterion("JCUPLOADFLAG <", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagLessThanOrEqualTo(String value) {
            addCriterion("JCUPLOADFLAG <=", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagLike(String value) {
            addCriterion("JCUPLOADFLAG like", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagNotLike(String value) {
            addCriterion("JCUPLOADFLAG not like", value, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagIn(List<String> values) {
            addCriterion("JCUPLOADFLAG in", values, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagNotIn(List<String> values) {
            addCriterion("JCUPLOADFLAG not in", values, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagBetween(String value1, String value2) {
            addCriterion("JCUPLOADFLAG between", value1, value2, "jcuploadflag");
            return (Criteria) this;
        }

        public Criteria andJcuploadflagNotBetween(String value1, String value2) {
            addCriterion("JCUPLOADFLAG not between", value1, value2, "jcuploadflag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}