package com.example.generator.mapper;

import com.example.generator.pojo.MwInsheets;
import com.example.generator.pojo.MwInsheetsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MwInsheetsMapper {
    long countByExample(MwInsheetsExample example);

    int deleteByExample(MwInsheetsExample example);

    int deleteByPrimaryKey(String insheetno);

    int insert(MwInsheets record);

    int insertSelective(MwInsheets record);

    List<MwInsheets> selectByExample(MwInsheetsExample example);

    MwInsheets selectByPrimaryKey(String insheetno);

    int updateByExampleSelective(@Param("record") MwInsheets record, @Param("example") MwInsheetsExample example);

    int updateByExample(@Param("record") MwInsheets record, @Param("example") MwInsheetsExample example);

    int updateByPrimaryKeySelective(MwInsheets record);

    int updateByPrimaryKey(MwInsheets record);
}