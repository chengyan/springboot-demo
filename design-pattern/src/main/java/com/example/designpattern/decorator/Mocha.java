package com.example.designpattern.decorator;

/**
 * Mocha 是一个装饰者，所以让它扩展自CondimentDecorator
 *
 */
public class Mocha extends CondimentDecorator {

    Beverage beverage;//用一个实例变量记录饮料，也就是被装饰者

    public Mocha(Beverage beverage){
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+",Mocha";
    }

    @Override
    public double cost() {
        return 8+beverage.cost();
    }
}
