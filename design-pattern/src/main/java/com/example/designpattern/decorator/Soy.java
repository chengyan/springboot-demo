package com.example.designpattern.decorator;


/**
 * 一种调料
 */
public class Soy  extends CondimentDecorator {

    Beverage beverage;

    public Soy(Beverage beverage){
        this.beverage = beverage;
    }

    @Override
    public String getDescription() {
        return beverage.getDescription()+",Soy";
    }

    @Override
    public double cost() {
        return 0.11+beverage.cost();
    }
}
