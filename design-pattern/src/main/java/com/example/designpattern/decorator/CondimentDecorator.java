package com.example.designpattern.decorator;

/**
 * 调料装饰者
 * 备注：之所以要继承Beverage,是因为装饰类要能够取待Beverage
 */
public abstract class CondimentDecorator extends Beverage {

    public abstract String getDescription();

}
