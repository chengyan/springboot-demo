package com.example.designpattern.decorator;


/**
 * 浓缩咖啡是一种饮料
 * Espresso 扩展自Beverage类，因为Espresso是一种饮料
 */
public class Espresso extends Beverage{

    public  Espresso(){
        description = "Espresso";
    }

    @Override
    public double cost() {
        return 1.99;
    }
}
