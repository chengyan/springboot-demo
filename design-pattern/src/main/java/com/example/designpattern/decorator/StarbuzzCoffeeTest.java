package com.example.designpattern.decorator;

public class StarbuzzCoffeeTest {

    public static void main(String[] args) {

        Beverage espresso = new Espresso();
        System.out.println("description:"+espresso.getDescription()+";price:"+espresso.cost());

        //装饰模式
        Beverage darkRoast = new Espresso();
        darkRoast = new Soy(darkRoast);//加 soy
        darkRoast = new Whip(darkRoast);//加 whip
        darkRoast = new Mocha(darkRoast);//加 mocha
        System.out.println("description:"+darkRoast.getDescription()+";price:"+darkRoast.cost());

    }
}
